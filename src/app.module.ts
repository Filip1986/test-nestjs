import { HttpModule, Module } from '@nestjs/common';
import { StoriesController } from './stories/stories.controller';
import { StoriesService } from './stories/stories.service';

@Module({
  imports: [HttpModule],
  controllers: [StoriesController],
  providers: [StoriesService],
})
export class AppModule {}
