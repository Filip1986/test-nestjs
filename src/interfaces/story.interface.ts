export interface Story {
  title: string;
  excerpt: string;
  origin: string;
  time: string;
  pictureLink: string;
  link: string;
}
