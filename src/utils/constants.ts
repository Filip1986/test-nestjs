/**
 * @author Luca Filip
 * @description Constants used throughout the application for easier refactoring
 * @namespace constants
 * @example public static EXAMPLE_CONSTANT = 'My example constant!';
 */

export const latest = '807';
export const binance = '1179';
export const bitcoin = '1176';
export const elrond = '1177';
export const etherium = '1178';

