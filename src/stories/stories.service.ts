import { HttpService, Injectable } from '@nestjs/common';
import { Story } from '../interfaces/story.interface';
import { AxiosResponse } from 'axios';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

@Injectable()
export class StoriesService {
  constructor(private httpService: HttpService) {
  }

  getStories(category: string): Observable<AxiosResponse<Story[]>> {
    return this.httpService.get(`https://news.maiar.com/wp-json/wp/v2/story?story_category=${category}`).pipe(
      map((res: AxiosResponse) => {
        return res.data.map(story => {
          return {
            title: story.title.rendered,
            excerpt: story.excerpt.rendered,
            origin: this.getHostnameFromUrl(story.link),
            time: this.getTime(story.modified),
            imgId: story.featured_media,
            pictureLink: story._links[`wp:featuredmedia`][0].href,
            link: story.link,
          };
        });
      }),

      mergeMap(stories => {
        const arrOfObs = stories.map(story => {
          return this.getPicture(story.pictureLink).pipe(catchError(error => {
            return of(error);
          }));
        });

        return forkJoin(arrOfObs).pipe(map(images => {
          return stories.map(story => {
            const image = images.find(image => image[`id`] === story.imgId);
            return {
              title: story.title,
              excerpt: story.excerpt,
              origin: story.origin,
              link: story.link,
              time: story.time,
              image: image ? image[`media_details`].sizes.thumbnail.source_url : null
            };
          });
        }));
      }),
    );
  }

  getPicture(url: string): Observable<AxiosResponse> {
    return this.httpService.get(url).pipe(map(res => {
      return res.data;
    }));
  }

  getTime(time: any): any {
    const present: any = new Date();
    const postTime: any = new Date(time);
    let text;

    // get total seconds between the times
    let delta = Math.abs(postTime - present) / 1000;

    // calculate (and subtract) whole days
    const days = Math.floor(delta / 86400);
    delta -= days * 86400;

    // calculate (and subtract) whole hours
    const hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;

    // calculate (and subtract) whole minutes
    const minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;

    // what's left is seconds
    const seconds = delta % 60;  // in theory the modulus is not required

    if (days) {
      if (days === 1) {
        text =  `${days} day ago`
      } else {
        text = `${days} days ago`
      }
      return text;
    }

    if (hours) {
      if (hours === 1) {
        text =  `${hours} hour ago`
      } else {
        text = `${hours} hours ago`
      }
      return text;
    }

    if (minutes) {
      if (minutes === 1) {
        text =  `${minutes} minute ago`
      } else {
        text = `${minutes} minutes ago`
      }
      return text;
    }

    if (seconds) {
      if (seconds === 1) {
        text =  `${seconds} minute ago`
      } else {
        text = `${seconds} minutes ago`
      }
      return text;
    }
  }

  getHostnameFromUrl(str: string): string {
    let hostname = new URL(str).hostname;
    if (hostname.indexOf('www') !== -1) {
      hostname = hostname.split('.')[1];
    } else {
      hostname = hostname.split('.')[0];
    }
    return hostname;
  }
}
