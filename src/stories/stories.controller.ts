import { Controller, Get } from '@nestjs/common';
import { StoriesService } from './stories.service';
import { Story } from '../interfaces/story.interface';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import { latest, binance, bitcoin, elrond, etherium } from '../utils/constants';

@Controller('stories')
export class StoriesController {
  constructor(private readonly storyService: StoriesService) {
  }

  @Get('latest')
  async getLatest(): Promise<Observable<AxiosResponse<Story[]>>> {
    return this.storyService.getStories(latest);
  }

  @Get('bitcoin')
  async getBitcoin(): Promise<Observable<AxiosResponse<Story[]>>> {
    return this.storyService.getStories(bitcoin);
  }

  @Get('binance')
  async getBinance(): Promise<Observable<AxiosResponse<Story[]>>> {
    return this.storyService.getStories(binance);
  }

  @Get('elrond')
  async getElrond(): Promise<Observable<AxiosResponse<Story[]>>> {
    return this.storyService.getStories(elrond);
  }

  @Get('etherium')
  async getEtherium(): Promise<Observable<AxiosResponse<Story[]>>> {
    return this.storyService.getStories(etherium);
  }

}
